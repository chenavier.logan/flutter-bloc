import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

const names = [
  'Aaliyah',
  'Aaron',
  'Abagail',
  'Abbey',
  'Abbie',
  'Abbigail',
  'Abby',
  'Abdiel',
  'Abdul',
  'Abdullah',
  'Abe',
  'Abel',
  'Abelardo',
  'Abigail',
  'Abigale',
  'Abigayle',
  'Abner',
  'Abraham',
  'Ada',
  'Adah',
  'Adalberto',
  'Adaline',
  'Adam',
  'Adan',
  'Addie',
  'Addison',
  'Adela',
  'Adelbert',
  'Adele',
  'Adelia',
  'Adeline',
  'Adell',
  'Adella',
  'Adelle',
  'Aditya',
  'Adolf',
  'Adolfo',
  'Adolph',
  'Adolphus',
  'Adonis',
  'Adrain',
  'Adrian',
  'Adriana',
  'Adrianna',
  'Adriel',
  'Adrien',
  'Adrienne',
  'Afton',
  'Aglae',
  'Agnes',
  'Agustin',
  'Agustina',
  'Ahmad',
  'Ahmed',
  'Aida',
  'Aidan',
  'Aideeeeeen',
  'Aileen',
  'Aimee',
  'Aisha',
  'Aiyana',
  'Akeem',
  'Al',
  'Alaina',
  'Alan',
  'Alana',
  'Alanis',
  'Alanna',
  'Alayna',
  'Alba',
  'Albert',
  'Alberta',
  'Albertha',
  'Alberto',
  'Albin',
  'Albina',
  'Alda',
  'Alden',
  'Alec',
  'Aleen',
  'Alejandra',
  'Alejandrin',
  'Alejandrina',
  'Alek',
  'Alena',
  'Alene',
  'Alessandra',
  'Alessandro',
  'Alessia',
  'Alessie',
  'Alex',
  'Alexa',
  'Alexander',
  ];

extension RandomElement<T> on Iterable<T> {
  T get randomElement => elementAt(Random().nextInt(length));
}

class NamesCubit extends Cubit<String?> {
  NamesCubit() : super(null);

  void pickRandomName() => emit(names.randomElement);
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late final NamesCubit cubit;

  @override
  void initState() {
    cubit = NamesCubit();
    super.initState();
  }

  @override
  void dispose() {
    cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: StreamBuilder<String?>(
        stream: cubit.stream,
        builder: (context, snapshot) {
          final button = TextButton(
              onPressed: () {
                cubit.pickRandomName();
              },
              child: const Text('Pick a random name'));
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return button;
            case ConnectionState.waiting:
              return const Center(child: CircularProgressIndicator());
            case ConnectionState.active:
            case ConnectionState.done:
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(snapshot.data ?? 'No name'),
                    button,
                  ],
                ),
              );
          }
        },
      ),
    );
  }
}
